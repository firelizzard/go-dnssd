// Copyright (c) 2019 The Go DNS-SD Authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/grandcat/zeroconf"
	"github.com/spf13/cobra"
)

func init() {
	rootCmd.AddCommand(discoverCmd)

	discoverCmd.Run = func(cmd *cobra.Command, args []string) {
		if len(args) == 0 {
			if *discoverInstance != "" && *discoverService == "" {
				discoverAll()
			} else {
				discover()
			}
			return
		}

		switch args[0] {
		case "all":
			discoverAll()
			return
		}

		cmd.Help()
	}
}

var discoverCmd = &cobra.Command{
	Use:   "discover",
	Short: "Discover services with DNS-SD",

	ValidArgs: []string{
		"all",
	},
	Args: func(cmd *cobra.Command, args []string) error {
		if err := cobra.MaximumNArgs(1)(cmd, args); err != nil {
			return err
		}
		if err := cobra.OnlyValidArgs(cmd, args); err != nil {
			return err
		}
		return nil
	},
}

var discoverInstance = discoverCmd.Flags().StringP("instance", "i", "", "Lookup records for a specific instance")
var discoverDuration = discoverCmd.Flags().DurationP("wait", "w", 10*time.Second, "DNS-SD response wait time")
var discoverDomain = discoverCmd.Flags().StringP("domain", "d", "local", "DNS-SD search domain")
var discoverService = discoverCmd.Flags().StringP("service", "s", "", "DNS-SD service to discover")

func discover() {
	ctx, cancel := context.WithTimeout(context.Background(), *discoverDuration)
	defer cancel()

	resolver, err := zeroconf.NewResolver(nil)
	if err != nil {
		log.Fatal(err)
	}

	w := new(sync.WaitGroup)
	w.Add(1)

	ch := make(chan *zeroconf.ServiceEntry)

	if *discoverInstance != "" {
		go func() {
			defer w.Done()
			for e := range ch {
				showEntry(e, false)
			}
		}()

		err = resolver.Lookup(ctx, *discoverInstance, *discoverService, *discoverDomain, ch)
		if err != nil {
			log.Fatal(err)
		}

		w.Wait()

	} else if *discoverService == "" {
		services := []string{}

		go func() {
			defer w.Done()
			for e := range ch {
				services = append(services, e.Instance)
				fmt.Print(".")
			}
		}()

		fmt.Println("# Browsing available services")
		err = resolver.Browse(ctx, "_services._dns-sd._udp", *discoverDomain, ch)

		if err != nil {
			log.Fatal(err)
		}

		w.Wait()
		fmt.Println()

		sort.Strings(services)
		for _, service := range services {
			parts := strings.Split(service, ".")
			if len(parts) < 3 {
				continue
			}
			for len(parts) > 3 {
				parts[1] = parts[0] + "." + parts[1]
				parts = parts[1:]
			}
			name, port, domain := parts[0], parts[1], parts[2]

			if strings.HasPrefix(name, "_") {
				name = name[1:]
			}

			switch port {
			case "_tcp":
				fmt.Printf("TCP %s in %s (%s)\n", name, domain, service)
			case "_udp":
				fmt.Printf("UDP %s in %s (%s)\n", name, domain, service)
			default:
				if strings.HasPrefix(port, "_") {
					port = port[1:]
				}
				port = strings.ToUpper(port)
				fmt.Printf("%s %s in %s (%s)\n", port, name, domain, service)
			}
		}

	} else {
		go func() {
			defer w.Done()
			for e := range ch {
				showEntry(e, false)
			}
		}()

		fmt.Printf("# Browsing %s\n", *discoverService)
		err = resolver.Browse(ctx, *discoverService, *discoverDomain, ch)
		if err != nil {
			log.Fatal(err)
		}

		w.Wait()
	}
}

func discoverAll() {
	if *discoverService != "" {
		fmt.Fprintf(os.Stderr, "Argument `all` is incompatible with flag `-s`\n")
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), *discoverDuration)
	defer cancel()

	resolver, err := zeroconf.NewResolver(nil)
	if err != nil {
		log.Fatal(err)
	}

	w1 := new(sync.WaitGroup)
	w1.Add(1)

	ch1 := make(chan *zeroconf.ServiceEntry)
	ch2 := make(chan *zeroconf.ServiceEntry)

	go func() {
		defer w1.Done()
		for e := range ch1 {
			e := e // local copy

			if strings.HasSuffix(e.Instance, "."+*discoverDomain) {
				e.Instance = e.Instance[:len(e.Instance)-len(*discoverDomain)-1]
			}

			w2 := new(sync.WaitGroup)
			w2.Add(1)

			ch := make(chan *zeroconf.ServiceEntry)

			go func() {
				defer w2.Done()
				for e := range ch {
					ch2 <- e
				}
			}()

			go func() {
				ctx, cancel := context.WithTimeout(context.Background(), *discoverDuration)
				defer cancel()

				var err error
				if *discoverInstance == "" {
					err = resolver.Browse(ctx, e.Instance, *discoverDomain, ch)
				} else {
					err = resolver.Lookup(ctx, *discoverInstance, e.Instance, *discoverDomain, ch)
				}
				if err != nil {
					log.Fatal(err)
				}

				w2.Wait()
			}()
		}
	}()

	go func() {
		for e := range ch2 {
			showEntry(e, true)
		}
	}()

	if *discoverInstance == "" {
		fmt.Println("# Browsing available services")
	} else {
		fmt.Printf("# Browsing available services of %s\n", *discoverInstance)
	}
	err = resolver.Browse(ctx, "_services._dns-sd._udp", *discoverDomain, ch1)
	if err != nil {
		log.Fatal(err)
	}

	w1.Wait()
	close(ch2)
}

func showEntry(e *zeroconf.ServiceEntry, includeService bool) {
	e.Instance = strings.ReplaceAll(e.Instance, `\ `, ` `)

	uq, err := strconv.Unquote(`"` + e.Instance + `"`)
	if err == nil {
		e.Instance = uq
	}

	if strings.HasSuffix(e.HostName, ".") {
		e.HostName = e.HostName[:len(e.HostName)-1]
	}

	fmt.Printf("\n%s (%s:%d)", e.Instance, e.HostName, e.Port)
	if includeService {
		fmt.Printf(" [%s]", e.Service)
	}
	fmt.Println()
	for _, txt := range e.Text {
		if txt == "" {
			continue
		}
		fmt.Printf("  %s\n", txt)
	}
	for _, ip := range e.AddrIPv4 {
		fmt.Printf("  %v\n", ip)
	}
	for _, ip := range e.AddrIPv6 {
		fmt.Printf("  %v\n", ip)
	}
}
