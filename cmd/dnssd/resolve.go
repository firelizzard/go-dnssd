// Copyright (c) 2019 The Go DNS-SD Authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"errors"
	"fmt"
	"log"
	"net"
	"time"

	"github.com/miekg/dns"
	"github.com/spf13/cobra"
	"gitlab.com/firelizzard/go-dnssd"
)

func init() {
	rootCmd.AddCommand(resolveCmd)

	resolveCmd.Run = func(cmd *cobra.Command, args []string) {
		resolve(args)
	}
}

var resolveCmd = &cobra.Command{
	Use:   "resolve HOST",
	Short: "Resolve hosts with mDNS",
	Args:  cobra.ExactArgs(1),
}

var resolveTimeout = resolveCmd.Flags().DurationP("timeout", "t", 10*time.Second, "Set mDNS timeout")

func resolve(args []string) {
	conn, err := dnssd.Dial()
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	dl := time.Now().Add(*resolveTimeout)
	conn.SetWriteDeadline(dl)
	conn.SetReadDeadline(dl)

	name := dns.Fqdn(args[0])
	err = conn.Send(&dns.Msg{
		MsgHdr: dns.MsgHdr{
			Id: dns.Id(),
		},
		Question: []dns.Question{
			dns.Question{Name: name, Qtype: dns.TypeA, Qclass: dns.ClassINET},
			dns.Question{Name: name, Qtype: dns.TypeAAAA, Qclass: dns.ClassINET},
		},
	})
	if err != nil {
		log.Fatal(err)
	}

	err = conn.Receive(nil, func(r *dns.Msg, src net.Addr) {
		if len(r.Answer) == 0 {
			return
		}
		for _, rr := range r.Answer {
			r := rr.Header()
			if r.Name != name {
				return
			}
			if r.Rrtype != dns.TypeA && r.Rrtype != dns.TypeAAAA {
				return
			}
		}
		fmt.Printf("# answer from %v\n%v\n", src, r)
	})
	if err != nil {
		var to interface{ Timeout() bool }
		if !errors.As(err, &to) || !to.Timeout() {
			log.Fatal(err)
		}
	}
}
