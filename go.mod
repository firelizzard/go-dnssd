module gitlab.com/firelizzard/go-dnssd

go 1.13

require (
	github.com/cenkalti/backoff v2.2.1+incompatible // indirect
	github.com/grandcat/zeroconf v0.0.0-20190424104450-85eadb44205c
	github.com/miekg/dns v1.1.26
	github.com/pkg/errors v0.8.1 // indirect
	github.com/spf13/cobra v0.0.5
	golang.org/x/net v0.0.0-20191209160850-c0dbc17a3553
)
