# DNS-SD

Utilities for DNS Service Discovery

## `dnssd browse`

Enumerate DNS-SD service types available on the network.

## `dnssd browse -s SERVICE`

Browse available SERVICEs on the network, for example `_http._tcp`.

## `dnssd resolve HOST`

Resolve HOST via mDNS.
