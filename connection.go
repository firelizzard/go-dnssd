// Copyright (c) 2019 The Go DNS-SD Authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package dnssd

import (
	"context"
	"errors"
	"net"
	"sync"
	"sync/atomic"
	"time"
	"unsafe"

	"github.com/miekg/dns"
	"golang.org/x/net/ipv4"
	"golang.org/x/net/ipv6"
)

var ipv4Listen = net.ParseIP("224.0.0.0")
var ipv6Listen = net.ParseIP("ff02::")
var ipv4Group = net.ParseIP("224.0.0.251")
var ipv6Group = net.ParseIP("ff02::fb")

type Conn struct {
	ipv4 *ipv4.PacketConn
	ipv6 *ipv6.PacketConn
	ifs  []net.Interface
}

func Dial() (*Conn, error) {
	ifs, err := net.Interfaces()
	if err != nil {
		return nil, err
	}

	uc4, err := net.ListenUDP("udp4", &net.UDPAddr{IP: ipv4Listen, Port: 5353})
	if err != nil {
		return nil, err
	}

	uc6, err := net.ListenUDP("udp6", &net.UDPAddr{IP: ipv6Listen, Port: 5353})
	if err != nil {
		return nil, err
	}

	conn := &Conn{
		ipv4: ipv4.NewPacketConn(uc4),
		ipv6: ipv6.NewPacketConn(uc6),
	}

	conn.ipv4.SetControlMessage(ipv4.FlagInterface, true)
	conn.ipv6.SetControlMessage(ipv6.FlagInterface, true)

	for i := range ifs {
		if ifs[i].Flags&net.FlagUp == 0 || ifs[i].Flags&net.FlagMulticast == 0 {
			continue
		}

		ok := false
		if err := conn.ipv4.JoinGroup(&ifs[i], &net.UDPAddr{IP: ipv4Group}); err == nil {
			ok = true
		}

		if err := conn.ipv6.JoinGroup(&ifs[i], &net.UDPAddr{IP: ipv6Group}); err == nil {
			ok = true
		}

		if ok {
			conn.ifs = append(conn.ifs, ifs[i])
		}
	}

	if len(conn.ifs) == 0 {
		conn.ipv4.Close()
		conn.ipv6.Close()
		return nil, errors.New("failed to join any interfaces")
	}

	return conn, nil
}

func (c *Conn) Close() error {
	c.ipv4.Close()
	c.ipv6.Close()
	return nil
}

func (c *Conn) writeTo4(b []byte, ifi int) (int, error) {
	cm := &ipv4.ControlMessage{IfIndex: ifi}
	addr := &net.UDPAddr{IP: ipv4Group, Port: 5353}
	return c.ipv4.WriteTo(b, cm, addr)
}

func (c *Conn) writeTo6(b []byte, ifi int) (int, error) {
	cm := &ipv6.ControlMessage{IfIndex: ifi}
	addr := &net.UDPAddr{IP: ipv6Group, Port: 5353}
	return c.ipv6.WriteTo(b, cm, addr)
}

func (c *Conn) Send(m *dns.Msg) error {
	b, err := m.Pack()
	if err != nil {
		return err
	}

	for i := range c.ifs {
		ifi := c.ifs[i].Index
		if _, err := c.writeTo4(b, ifi); err != nil {
			_ = err
			// return err
		}
		if _, err := c.writeTo6(b, ifi); err != nil {
			_ = err
			// return err
		}
	}

	return nil
}

func (c *Conn) readFrom4(b []byte) (int, net.Addr, error) {
	n, _, src, err := c.ipv4.ReadFrom(b)
	return n, src, err
}

func (c *Conn) readFrom6(b []byte) (int, net.Addr, error) {
	n, _, src, err := c.ipv6.ReadFrom(b)
	return n, src, err
}

func (c *Conn) readMsg(op func([]byte) (int, net.Addr, error)) (*dns.Msg, net.Addr, error) {
	var b [1 << 16]byte

	n, src, err := op(b[:])
	if err != nil {
		return nil, nil, err
	}

	msg := new(dns.Msg)
	err = msg.Unpack(b[:n])
	if err != nil {
		return nil, nil, err
	}

	return msg, src, nil
}

type errw struct {
	err           error
	temp, timeout bool
}

func (err *errw) Error() string {
	return err.err.Error()
}

func (err *errw) Temporary() bool {
	return err.temp
}

func (err *errw) Timeout() bool {
	return err.timeout
}

func (c *Conn) Receive(ctx context.Context, fn func(*dns.Msg, net.Addr)) error {
	if ctx == nil {
		ctx = context.Background()
	}
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	var finalErr *error
	errPtr := unsafe.Pointer(finalErr)

	w := new(sync.WaitGroup)
	w.Add(2)

	go func() {
		defer w.Done()
		defer cancel()

		for {
			msg, src, err := c.readMsg(c.readFrom4)
			if err != nil {
				atomic.CompareAndSwapPointer(&errPtr, nil, unsafe.Pointer(&err))
				return
			}

			fn(msg, src)
		}
	}()

	go func() {
		defer w.Done()
		defer cancel()

		for {
			msg, src, err := c.readMsg(c.readFrom6)
			if err != nil {
				atomic.CompareAndSwapPointer(&errPtr, nil, unsafe.Pointer(&err))
				return
			}

			fn(msg, src)
		}
	}()

	<-ctx.Done()
	// errDone := errors.New("done")
	atomic.CompareAndSwapPointer(&errPtr, nil, unsafe.Pointer(new(error)))
	c.SetReadDeadline(time.Now())

	w.Wait()

	if finalErr == nil {
		return nil
	}

	err := *finalErr
	if err == nil {
		return nil
	}

	var tmp interface{ Temporary() bool }
	var to interface{ Timeout() bool }
	return &errw{
		err:     err,
		temp:    errors.As(err, &tmp) && tmp.Temporary(),
		timeout: errors.As(err, &to) && to.Timeout(),
	}
}

func (c *Conn) SetDeadline(t time.Time) error {
	if err := c.ipv4.SetDeadline(t); err != nil {
		return err
	}
	return c.ipv6.SetDeadline(t)
}

func (c *Conn) SetReadDeadline(t time.Time) error {
	if err := c.ipv4.SetReadDeadline(t); err != nil {
		return err
	}
	return c.ipv6.SetReadDeadline(t)
}

func (c *Conn) SetWriteDeadline(t time.Time) error {
	if err := c.ipv4.SetWriteDeadline(t); err != nil {
		return err
	}
	return c.ipv6.SetWriteDeadline(t)
}
